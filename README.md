# Programming Mermaid

```mermaid
graph TD
    A[I want to learn be a programmer]-->Language[Programming Language]

    Language --> JS(Javascript)
    JS --> Express(Express)
    JS --> React(React)
    JS --> Angular(Angular)
    JS --> Promises(Promises)

    Language -->  J(Java)
    J --> Spring(SpringBoot)
    Spring --> SJPA(Data JPA)
    Spring --> SThymeleaf(Thymeleaf)
    Spring --> SSecurity(Spring Security)
    SJPA --> Databases
    J --> JRX(ReactiveX)

    Language --> P(Python)
    P --> Django(Django)

    A --> Databases[Databases]
    Databases --> Mysql(MySQL)
    Databases --> Mongo(MongoDB)
    Databases --> Redis(Redis)
    Databases --> DynamoDB

    A --> AWS(AWS)
    AWS --> S3(S3)
    AWS --> DynamoDB(DynamoDB)
    AWS --> Lambda(Lambda)
    AWS --> EC2(EC2)

    A --> Web(Web)
    Web --> CSS(CSS)
    CSS --> SCsS(SCSS)
    Web --> HTML(HTML)
    Web --> PWA(PWA)
```
